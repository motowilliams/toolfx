async function handleRequest(request) {
  // Return a new Response based on a URL's hostname
  const url = new URL(request.url)
  console.log(`url ${url}`)

  const contentType = request.headers.get("Content-Type") || ""
  console.log(`contentType ${contentType}`)

  const userAgent = request.headers.get("User-Agent") || ""
  console.log(`userAgent ${userAgent}`)

  var init = {}

  console.log(`Loading user agent script from ${script_config_uri}`)
  const configResponse = await fetch(script_config_uri, init)
  var userAgentScripts = JSON.parse(await configResponse.text());

  for (var key in userAgentScripts) {
    if (userAgent.includes(key)) {
      scriptUrl = userAgentScripts[key]
      console.log(key + ":" + scriptUrl);
      init = {
        headers: {
          "content-type": "text/plain",
        },
      }
      return await fetch(scriptUrl, init)
    }
  }

  console.log(`Pass-Thru to ${url} as Content-Type ${contentType}`)
  init = {
    headers: request.headers,
  }
  return response = await fetch(url, init)
}

addEventListener("fetch", event => {
  event.respondWith(handleRequest(event.request))
})
